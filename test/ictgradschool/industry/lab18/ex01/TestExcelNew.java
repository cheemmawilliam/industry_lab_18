package ictgradschool.industry.lab18.ex01;

import org.junit.Test;
import org.junit.Before;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Write tests
 */
public class TestExcelNew {

    ExcelNew excelNew = new ExcelNew(550);

    @Test
    public void testGetStudentId() {

        assertEquals(excelNew.getStudentId(50),"0050");
        assertEquals(excelNew.getStudentId(100),"0100");

    }

    @Test
    public void testGetClassMarks() {

        assertTrue("\t79\t83\t80\t65\t50", true);
        assertTrue("100\t96\t92\t96\t90",true);
        assertFalse("46\t45\t45\t47\t25\t45",false);
        assertFalse("52\t55\t58",false);


    }

     @Test
    public void testGetStudentMarks(){
        int[] labScheme = {5, 15, 25, 65};
        int[] testScheme = {5, 20, 65, 90};

        assertFalse(excelNew.getMark(4,labScheme) == null);
        assertFalse(excelNew.getMark(50,testScheme) == null);
        
    }









}