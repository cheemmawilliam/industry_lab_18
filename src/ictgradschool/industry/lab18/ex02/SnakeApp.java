package ictgradschool.industry.lab18.ex02;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 * TODO Have fun :)
 */
public class SnakeApp extends JFrame {

    private static final Random random = new Random();
    private Canvas canvas = new Canvas();
    private int xGreen = -1, yGreen = -1;
    private ArrayList<Integer> xsquRed = new ArrayList<>();
    private ArrayList<Integer> xsquSnake = new ArrayList<>();
    private ArrayList<Integer> ysquRed = new ArrayList<>();
    private ArrayList<Integer> ysquSnake = new ArrayList<>();
    private int direction;
    private boolean done = false;

    public static void main(String[] args) {
        new SnakeApp().go();
    }

    public SnakeApp() {

        for (int i = 0; i < 6; i++) {
            xsquSnake.add(10 - i);
            ysquSnake.add(10);
        }

        this.direction = 39;

        setTitle("SnakeApp" + " : " + 6);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(200, 200, 30 * 25 + 6, 20 * 25 + 28);
        setResizable(false);
        canvas.setBackground(Color.white);
        add(BorderLayout.CENTER, canvas);

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {

                int d1 = e.getKeyCode();

                if ((d1 >= 37) && (d1 <= 40)) {// block wrong codes
                    if (Math.abs(SnakeApp.this.direction - d1) != 2) {// block moving back
                        SnakeApp.this.direction = d1;
                    }
                }

            }
        });

        setVisible(true);
    }

    private void go() { // main loop
        while (!done) {

            int xHead = xsquSnake.get(0);
            int yHead = ysquSnake.get(0);


            if (direction == 37) {
                xHead--;
            }
            if (direction == 39) {
                xHead++;
            }
            if (direction == 38) {
                yHead--;
            }
            if (direction == 40) {
                yHead++;
            }
            if (xHead > 30 - 1) {
                xHead = 0;
            }
            if (xHead < 0) {
                xHead = 30 - 1;
            }
            if (yHead > 20 - 1) {
                yHead = 0;
            }
            if (yHead < 0) {
                yHead = 20 - 1;
            }

            done = overLapError(xsquRed, ysquRed, xHead, yHead,false) || overLapError(xsquSnake, ysquSnake, xHead, yHead,true);

            xsquSnake.add(0, xHead);
            ysquSnake.add(0, yHead);

            if ((checkPosition(xsquSnake, ysquSnake, xGreen, yGreen, 0))) {
                xGreen = -1;
                yGreen = -1;
                setTitle("SnakeApp" + " : " + xsquSnake.size());
            } else {
                xsquSnake.remove(xsquSnake.size() - 1);
                ysquSnake.remove(ysquSnake.size() - 1);
            }

            if (xGreen == -1) {
                Square greenSquare = new Square(false);
                xGreen = greenSquare.getX();
                yGreen = greenSquare.getY();

                Square redSquare = new Square(true);
                xsquRed.add(redSquare.getX());
                ysquRed.add(redSquare.getY());
            }

            canvas.repaint();

            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean overLapError(ArrayList<Integer> xs, ArrayList<Integer> ys, int x, int y, boolean doit) {
        for (int i = 0; i < xs.size(); i++) {
            if (checkPosition(xs, ys, x, y, i)) {
                if (doit) {if (!(checkPosition(xs, ys, x, y,xs.size() - 1,ys.size() - 1))) return true;}
                else return true;
            }
        }
        return false;
    }

    private boolean checkPosition(ArrayList<Integer> xSquare, ArrayList<Integer> ySquare, int x, int y, int i) {
        return checkPosition(xSquare, ySquare, x, y, i, i);
    }

    private boolean checkPosition(ArrayList<Integer> xSquare, ArrayList<Integer> ySquare, int x, int y, int ix, int iy) {
        return xSquare.get(ix) == x && ySquare.get(iy) == y;
    }

    private class Canvas extends JPanel {
        @Override
        public void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);
            fillColor3DRAll(graphics, Color.GRAY, xsquSnake, ysquSnake);
            fillColor3DR(graphics, Color.green, xGreen, yGreen);
            fillColor3DRAll(graphics, Color.red, xsquRed, ysquRed);

            if (done) {
                graphics.setColor(Color.red);
                graphics.setFont(new Font("Arial", Font.BOLD, 60));
                FontMetrics fm = graphics.getFontMetrics();
                graphics.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
            }
        }

        private void fillColor3DRAll(Graphics graphics, Color color, ArrayList<Integer> xs, ArrayList<Integer> ys) {
            for (int i = 0; i < xs.size(); i++) {
                fillColor3DR(graphics, color, xs.get(i), ys.get(i));
            }
        }

        private void fillColor3DR(Graphics graphics, Color color, int x, int y) {
            graphics.setColor(color);
            graphics.fill3DRect(x * 25 + 1, y * 25 + 1, 25 - 2, 25 - 2, true);
        }
    }

    private class Square {
        private int x;
        private int y;
        private int count = 0;

        Square(boolean isRed) {
            do {
                System.out.println("Count " + count++ + ": " + x + "," + y);
                x = random.nextInt(30);
                y = random.nextInt(20);
                if (overLapError(xsquRed, ysquRed, x, y,false))continue;
                else if (overLapError(xsquSnake, ysquSnake, x, y, true))continue;
                else if (!(isRed && xGreen == x && yGreen == y)) break;
            } while (true);
        }

        int getX() {return x;}

        int getY() {return y;}
    }
}